app.controller('PostsEditCtrl', function ($scope, Posts, $stateParams, alertService, $log, $window, $state) {

  $scope.post = {};

  Posts.get($stateParams.id).then(function (data) {
    $scope.post = data;
  });

  $scope.editPost = function () {
    var post = $scope.post;

    Posts.edit(post).then(function () {
      $state.go('post', { id: post._id });
      alertService.add("success", "Well done! You successfully edited your post.");
    });

    alertService.clear();
  };

});
