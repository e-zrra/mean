app.controller('PostsShowCtrl', function ($scope, Posts, auth, $stateParams, alertService, $log, $window, $state) {

  $scope.post = {};
  $scope.isLoggedIn = auth.isLoggedIn;
  $scope.currentUser = auth.currentUser();

  Posts.get($stateParams.id).then(function (data) {
    $scope.post = data;
    $scope.showBtn = function () {
      if ($scope.post.author.username === $scope.currentUser) { return true; }
      return false;
    };
  });

  Array.prototype.indexOfObject = function arrayObjectIndexOf(property, value) {
    for (var i = 0, len = this.length; i < len; i++) {
        if (this[i][property] === value) return i;
    }
    return -1;
  }

  $scope.incrementUpvotes = function (post) {
    Posts.upvote(post);
  };

  $scope.openCommentPrompt = function (refObject,refId) {

    var commentText = prompt("Comment");

    if (commentText) {
      var newComment = { body: commentText };

		  if (refObject == 'post') newComment.post = refId;
  		if (refObject == 'comment') newComment.comment= refId;

      Posts.addComment(newComment).then(function(comment) {

        if (refObject == 'comment') {
          var index = $scope.post.comments.indexOfObject("_id", refId);

          if (index > -1) {
            var newComment = comment.data;
            newComment["author"] = {};
            newComment.author.username = $scope.currentUser;
            $scope.post.comments[index].responses.unshift(newComment);
          }
    		}

        if (refObject == 'post') {
          comment.author = $scope.currentUser;
          $scope.post.comments.push(comment.data)
        }

  		});
    }
  };

  $scope.deletePost = function () {

    var res = confirm("Are you sure to delete this posts");

    if (res == true) {
      var post = $scope.post;

      Posts.delete(post).then(function () {
        $state.go('posts');
        alertService.add("success", "Well done! You successfully deleted your post.");
      });
    }
  }

});
