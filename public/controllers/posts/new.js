app.controller('PostsNewCtrl', function ($scope, Posts, auth, alertService, $state) {

  $scope.post = {};

  $scope.isLoggedIn = auth.isLoggedIn;

	$scope.newPost = function () {

		Posts
      .create($scope.post)
      .then(function () {

			  $state.go('home');

        alertService.add("success", "Well done! You successfully added a new post.");

		});

		alertService.clear();
	};

});
