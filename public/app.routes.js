app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('home', {
				url: '/home',
				templateUrl: 'views/home.html',
				controller: 'PostsCtrl'
			})
			.state('posts', {
				url: '/posts',
				templateUrl: 'views/posts/index.html',
				controller: 'PostsCtrl'
			})
			.state('new-post', {
				url: '/new-post',
				templateUrl: 'views/posts/new.html',
				controller: 'PostsNewCtrl'
			})
			.state('edit-post', {
				url: '/post/{id}/edit',
				templateUrl: 'views/posts/edit.html',
				controller: 'PostsEditCtrl'
			})
			.state('post', {
				url: '/post/{id}',
				templateUrl: 'views/posts/post.html',
				controller: 'PostsShowCtrl'
			})
			.state('contact', {
				url: '/contact',
				templateUrl: 'views/contact.html',
				controller: 'AuthController',
			})
			.state('login', {
			  url: '/login',
			  templateUrl: 'views/auth/login.html',
			  controller: 'AuthController',
			  onEnter: ['$state', 'auth', function ($state, auth) {
			    if(auth.isLoggedIn()){
			      $state.go('home');
			    }
			  }]
			})
			.state('register', {
			  url: '/register',
			  templateUrl: 'views/auth/register.html',
			  controller: 'AuthController',
			  onEnter: ['$state', 'auth', function($state, auth){
			    if(auth.isLoggedIn()){
			      $state.go('home');
			    }
			  }]
			})
			$urlRouterProvider.otherwise('home');

	}]);
