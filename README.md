# MEAN-Stack-Blog

## Blog SPA:

* User authentication.
* Creating, editing and deleting posts, voting posts.
* Creating comments, replying to comments.
* Bootstrap template integrated.

### Build with MEAN.js stack:

MongoDB
Express.js
AngularJS
Node.js

Install and run
```
npm install
npm start
```

Access: http://localhost:3000/

Make sure your mongodb is working.
```
$ mongod
```

References:
* https://github.com/inaabadjieva/MEAN-Stack-Blog
* https://github.com/jermspeaks/Thinkster-MEAN-Tutorial
