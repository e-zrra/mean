var mongoose = require('mongoose');
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');
var posts = {};

// GET Posts
posts.get = function (req, res, next) {
  Post.find().populate('author').sort({createdOn: -1}).exec(function(err, posts) {
		if(err) { return next(err); }

		res.json(posts);
	});
}

// POST New
posts.new = function (req, res, next) {
  if(!req.body.title || !req.body.body ) {
		return res.status(400).json({ message: 'Please fill out all fields.'});
	}
	var post = new Post(req.body);

	post.author = req.payload._id;

	post.save(function (err, post) {
	  if(err){ return next(err); }
		res.json(post);
	});
}

// GET Post
posts.show = function (req, res, next) {
  var id = req.params.post;
	var query = Post.findById(id);

	query.exec(function(err, post) {

		if(err) { return next(err); }
		if(!post) { return next(new Error('can\'t find post')); }

		post.populate([{
			path: 'author',
			model: 'User',
		},
		{
			path: 'comments',
			model: 'Comment',
			populate: [{
					path: 'author',
					model: 'User'
				},
				{
					path: 'responses',
					model: 'Comment',
					populate: {
					path: 'author',
					model: 'User'
				},
			}]
		}],
		 function(err, post) {
			if(err) { return next(err); }
			res.json(post);
		});

	});
}

// PUT Post
posts.upvote = function (req, res, next) {
  var id = req.params.post;
	var query = Post.findById(id);

  query.exec(function(err, post) {

		if(err) { return next(err); }
		if(!post) { return next(new Error('can\'t find post')); }

    post.upvote(function(err, post) {
  		if(err) { return next(err); }
  		res.json(post);
  	});
  });
}

// POST Comment
posts.comments = function (req, res, next) {
  var comment = new Comment(req.body);
  comment.author = req.payload._id;
  comment.save(function(err, comment) {
    if(err) { return next(err); }
    if(req.body.post) {
      Post.findById(req.body.post).exec(function(err,post){
        if(err) { return next(err)}
        post.comments.push(comment._id)
        post.save();
      })
    }
    if(req.body.comment) {
      Comment.findById(req.body.comment).exec(function(err,resComment){
        if(err) { return next(err)};
        resComment.responses.push(comment._id)
        resComment.save();
      })
    }
    res.json(comment);
  });
}

// PUT Post
posts.edit = function (req, res, next) {
  if(!req.body.title || !req.body.body ) {
		return res.status(400).json({ message: 'Please fill out all fields.'});
	}
	var post = req.body;

	Post.findByIdAndUpdate(req.body._id, post, function(err, result) {
  	if (err) { return next(err)};

  	res.json(result);
	});
}

// Delete Post
posts.delete = function (req, res, next) {
  Post.findByIdAndRemove(req.params.id, function(err) {
    if (err) { return next(err)};
    res.json("deleted");
  });
}

module.exports = posts;
