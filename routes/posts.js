var express = require('express');
var router = express.Router();
var $q = require('q');
var auth = require('../config/jwt');
var posts = require('../controllers/posts');

// GET Posts
router.get('/posts', posts.get);

// POST Post
router.post('/new-post', auth, posts.new);

// GET Post
router.get('/posts/:post', posts.show);

// UPVOTE Post
router.put('/posts/:post/upvote', posts.upvote);

// POST Comment
router.post('/comments', auth, posts.comments);

// EDIT Post
router.put('/edit-post', auth, posts.edit);

// DELETE Post
router.delete('/delete-post/:id', auth, posts.delete);

module.exports = router;
