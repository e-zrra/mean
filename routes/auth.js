var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var passport = require('passport');
var $q = require('q');

// Register
router.post('/register', function(req, res, next) {

	if(!req.body.fullname || !req.body.email || !req.body.username || !req.body.password  || !req.body.confirmPassword) {
		return res.status(400).json({ message: 'Please fill out all fields.'});
	}

	if(req.body.password  !== req.body.confirmPassword) {
		return res.status(400).json({ message:"Passwords do not match. Please enter your password again." , type : "danger" });
	}

	var result = $q.defer()

	User.find({ username: req.body.username }).exec(function(err, users) {
		if(users.length > 0){
			result.reject({ message:"This username already exist. Please enter another username." , type : "danger" })
		}else {
			result.resolve()
		}
	});

	User.find({email: req.body.email}).exec(function(err, users) {

		if(users.length > 0) {

			result.reject({message:"There is already a registered user with this email . Please enter another email." , type : "danger"})
		} else {
			result.resolve()
		}
	});

	result.promise.then(function(){
		var user = new User();
		user.fullname = req.body.fullname;
		user.username = req.body.username;
		user.email = req.body.email;
		user.setPassword(req.body.password);

		user.save(function(err) {
			if(err) { return next(err); }
			return res.json({ token: user.generateJWT()})
		});
	}).fail(function(err){
		return res.status(400).json(err)
	});

});

// Login
router.post('/login', function(req, res, next) {

	if(!req.body.username || !req.body.password) {
		return res.status(400).json({ message: 'Please fill out all fields'});
	}

	passport.authenticate('local', function(err, user, info){
		if(err) { return next(err); }
		if(user) {
			return res.json({ token: user.generateJWT()});
		} else {
			return res.status(401).json(info);
		}
	})(req, res, next);
	
});

module.exports = router;
